INTRODUCTION
------------

The Customer Data Storage module allows storing and retrieving customer data in 
a plugable way. The module provides the following storages:

* Example Data


* For a full description of the module, visit the project page:
  https://www.drupal.org/project/cds

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/cds

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

There is a configuration form available at `/admin/config/system/cds` where the
storages can be enabled and sorted. If a plugin has a configuration form, there
will be a link to its configuration form.

USAGE
-----

The module provides a handler that has 2 methods to return the storage settings.

* `getStorages()` returns the enabled storages sorted by their weight
* `getPrimaryStorage()` returns the first enabled storage

MAINTAINERS
-----------

Current maintainers:
* Tim Diels (tim-diels) - https://www.drupal.org/user/2915097
* Steven Van den Hout (svdhout) - https://www.drupal.org/user/717826

This project has been sponsored by:
* Calibrate
  Visit https://www.calibrate.be for more information.