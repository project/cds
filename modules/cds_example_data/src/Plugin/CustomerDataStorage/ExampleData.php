<?php

namespace Drupal\cds_example_data\Plugin\CustomerDataStorage;

use Drupal\cds\CustomerDataStoragePluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the customer data storage.
 *
 * @CustomerDataStorage(
 *   id = "example_data",
 *   weight = -1,
 *   name = @Translation("Example data"),
 *   description = @Translation("CDS based on Example data."),
 * )
 */
class ExampleData extends CustomerDataStoragePluginBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getField(string $field): string {
    switch ($field) {
      case 'email':
        return 'john.doe@examle.com';

      case 'firstName':
        return 'John';

      case 'lastName':
        return 'Doe';

      case 'phone':
        return '0123456789';

      default:
        return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setField(string $field, string $data) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function getAllFields(): array {
    return [
      'email' => $this->t('E-mail'),
      'phone' => $this->t('Phone'),
      'firstName' => $this->t('First name'),
      'lastName' => $this->t('Last name'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSegments(): array {
    return [
      'foodlover',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function addSegment(string $segment) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function getAllSegments(): array {
    return [
      'foodlover' => $this->t('Food Lover'),
      'musiclover' => $this->t('Music Lover'),
    ];
  }

}
