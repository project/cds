<?php

namespace Drupal\cds\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a customer date storage object.
 *
 * Plugin Namespace: Plugin\CustomerDataStorage
 *
 * For a working example, see
 * \Drupal\cds\Plugin\CustomerDataStorage\Cookie.
 *
 * @Annotation
 */
class CustomerDataStorage extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The default weight of the plugin.
   *
   * @var int
   */
  public $weight;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
