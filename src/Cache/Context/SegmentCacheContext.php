<?php

namespace Drupal\cds\Cache\Context;

use Drupal\cds\CustomerDataStorageHandlerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;

/**
 * Defines the SegmentCacheContext service.
 *
 * @DCG
 * Check out the core/lib/Drupal/Core/Cache/Context directory for examples of
 * cache contexts provided by Drupal core.
 */
class SegmentCacheContext implements CacheContextInterface
{

  protected $cds;

  public function __construct(CustomerDataStorageHandlerInterface $cds) {
    $this->cds = $cds->getStorageInstance($cds->getPrimaryStorage());
  }
  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('CDS Segments');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($segments = $this->cds->getSegments()) {
      return implode('-', $segments);
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }
}
