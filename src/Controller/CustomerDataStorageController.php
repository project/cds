<?php

namespace Drupal\cds\Controller;

use Drupal\cds\CustomerDataStorageHandlerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CustomerDataStorageController implements ContainerInjectionInterface {

  /**
   * The customer data storage interface.
   *
   * @var \Drupal\cds\CustomerDataStoragePluginInterface
   */
  protected $cds;

  /**
   * The controller constructor.
   *
   * @param \Drupal\cds\CustomerDataStorageHandlerInterface $cds
   *   The storage handler service.
   */
  public function __construct(CustomerDataStorageHandlerInterface $cds) {
    $this->cds = $cds->getStorageInstance($cds->getPrimaryStorage());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cds')
    );
  }

  /**
   * Get the value from a field.
   *
   * @param string $field
   *   The field.
   */
  public function get($field) {
    return new JsonResponse([
      'data' => $this->cds->getField($field),
      'method' => 'GET',
    ]);
  }

  /**
   * Get the segments.
   */
  public function getSegments() {
    return new JsonResponse([
      'data' => $this->cds->getSegments(),
      'method' => 'GET',
    ]);
  }

  /**
   * Get all the fields.
   */
  public function getAllFields() {
    return new JsonResponse([
      'data' => $this->cds->getAllFields(),
      'method' => 'GET',
    ]);
  }

  /**
   * Get all the segments.
   */
  public function getAllSegments() {
    return new JsonResponse([
      'data' => $this->cds->getAllSegments(),
      'method' => 'GET',
    ]);
  }
}