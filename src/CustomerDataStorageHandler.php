<?php

namespace Drupal\cds;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Handles customer data storage.
 */
class CustomerDataStorageHandler implements CustomerDataStorageHandlerInterface {

  /**
   * The storage plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $storageManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The settings instance.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * The request stack object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Local cache for storage plugin instances.
   *
   * @var array
   */
  protected $plugins;

  /**
   * Constructs a new CustomerDataStorageHandler object.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $storage_manager
   *   The storage plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings instance.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   */
  public function __construct(PluginManagerInterface $storage_manager, ConfigFactoryInterface $config_factory, Settings $settings, RequestStack $requestStack) {
    $this->storageManager = $storage_manager;
    $this->configFactory = $config_factory;
    $this->settings = $settings;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $this->plugins = [];
  }

  /**
   * Gets enabled storages.
   *
   * @return array
   *   An array of enabled storages.
   */
  protected function getEnabledStorages(): array {
    $storages = [];

    $storages_enabled = $this->configFactory->get('cds.settings')->get('storages.enabled');
    if (!empty($storages_enabled)) {
      foreach (array_keys($storages_enabled) as $storage_enabled) {
        $storages[$storage_enabled] = $this->storageManager->getDefinition($storage_enabled);
      }
    }

    return $storages;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstalledStorages(): array {
    return $this->storageManager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getStorages(): array {
    $definitions = $this->storageManager->getDefinitions();
    $enabled_storages = $this->getEnabledStorages();
    return array_intersect_key($definitions, $enabled_storages);
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageInstance(string $storage_id): CustomerDataStoragePluginInterface {
    if (!isset($this->plugins[$storage_id])) {
      $instance = $this->storageManager->createInstance($storage_id, []);
      $instance->setConfig($this->configFactory);
      $this->plugins[$storage_id] = $instance;
    }

    return $this->plugins[$storage_id];
  }

  /**
   * {@inheritdoc}
   */
  public function getPrimaryStorage(): string {
    $enabled_storages = $this->getEnabledStorages();
    return empty($enabled_storages) ? CustomerDataStorageHandlerInterface::STORAGE_ID : key($enabled_storages);
  }

  /**
   * {@inheritdoc}
   */
  public function isStorageEnabled(string $storage_id): bool {
    $enabled = FALSE;

    $enabled_storages = $this->getEnabledStorages();
    if (isset($enabled_storages[$storage_id])) {
      $enabled = TRUE;
    }

    return $enabled;
  }

}
