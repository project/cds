<?php

namespace Drupal\cds;

/**
 * Interface for the customer data storage handler.
 */
interface CustomerDataStorageHandlerInterface {

  const STORAGE_ID = 'example_data';

  /**
   * Resets the storages and the plugin instances.
   */
  public function reset();

  /**
   * Returns the installed customer data storage plugins.
   *
   * @return array[]
   *   An array of customer data storage plugin definitions keyed by plugin id.
   */
  public function getInstalledStorages(): array;

  /**
   * Returns the enabled customer data storage plugins.
   *
   * @return array[]
   *   An array of customer data storage plugin definitions keyed by plugin id.
   */
  public function getStorages(): array;

  /**
   * Returns an instance of the specified customer data storage plugin.
   *
   * @param string $storage_id
   *   The plugin identifier.
   *
   * @return \Drupal\cds\CustomerDataStoragePluginInterface
   *   The instance of the plugin.
   */
  public function getStorageInstance(string $storage_id): CustomerDataStoragePluginInterface;

  /**
   * Returns the ID of the primary storage plugin.
   *
   * @return string
   *   The identifier of the primary storage plugin, or the default plugin if
   *   none exists.
   */
  public function getPrimaryStorage(): string;

  /**
   * Checks whether a storage plugin is enabled.
   *
   * @param string $storage_id
   *   The storage plugin ID.
   *
   * @return bool
   *   TRUE if the plugin is enabled, or FALSE otherwise.
   */
  public function isStorageEnabled(string $storage_id): bool;

}
