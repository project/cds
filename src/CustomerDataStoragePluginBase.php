<?php

namespace Drupal\cds;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Base class for customer data storage plugins.
 */
abstract class CustomerDataStoragePluginBase implements CustomerDataStoragePluginInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function setConfig(ConfigFactoryInterface $config) {
    $this->config = $config;
  }

}
