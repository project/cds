<?php

namespace Drupal\cds;

/**
 * Interface for customer data storage classes.
 */
interface CustomerDataStoragePluginInterface {

  /**
   * Returns the data for a given field.
   *
   * @param string $field
   *   The field.
   *
   * @return string
   *   The data for the given field or ''.
   */
  public function getField(string $field): string;

  /**
   * Set the data for a given field.
   *
   * @param string $field
   *   The field.
   * @param string $data
   *   The data.
   */
  public function setField(string $field, string $data);

  /**
   * Returns all the fields.
   *
   * @return array
   *   All the fields and their info.
   */
  public function getAllFields(): array;

  /**
   * Returns the segments.
   *
   * @return array
   *   The segments.
   */
  public function getSegments(): array;

  /**
   * Add a segment.
   *
   * @param string $segment
   *   The segment.
   */
  public function addSegment(string $segment);

  /**
   * Returns all the segments.
   *
   * @return array
   *   The segments and their info.
   */
  public function getAllSegments(): array;

}
