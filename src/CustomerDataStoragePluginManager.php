<?php

namespace Drupal\cds;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages customer data storage plugins.
 */
class CustomerDataStoragePluginManager extends DefaultPluginManager {

  /**
   * Constructs a new CustomerDataStoragePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   An object that implements CacheBackendInterface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   An object that implements ModuleHandlerInterface.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/CustomerDataStorage', $namespaces, $module_handler, 'Drupal\cds\CustomerDataStoragePluginInterface', 'Drupal\cds\Annotation\CustomerDataStorage');
    $this->cacheBackend = $cache_backend;
    $this->cacheKeyPrefix = 'customer_data_storage_plugins';
    $this->cacheKey = 'customer_data_storage_plugins';
    $this->alterInfo('customer_data_storage_info');
  }

}
