<?php

namespace Drupal\cds\Form;

use Drupal\cds\CustomerDataStorageHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Customer Data Storage settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The customer data storage handler.
   *
   * @var \Drupal\cds\CustomerDataStorageHandlerInterface
   */
  protected $customerDataStorageHandler;

  /**
   * Constructs a SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cds\CustomerDataStorageHandlerInterface $customer_data_storage_handler
   *   The customer data storage handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CustomerDataStorageHandlerInterface $customer_data_storage_handler) {
    parent::__construct($config_factory);
    $this->customerDataStorageHandler = $customer_data_storage_handler;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cds')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cds_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cds.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cds.settings');
    $storages = $config->get('storages');
    $storages_enabled = $storages['enabled'] ?: [];
    $storages_weights = $storages['weights'] ?: [];

    // Add missing data to the list.
    $cds_options = $this->customerDataStorageHandler->getInstalledStorages();
    foreach ($cds_options as $cds_option_id => $cds_option) {
      if (!isset($storages_weights[$cds_option_id])) {
        $storages_weights[$cds_option_id] = $cds_option['weight'] ?: 0;
      }
    }

    $form['storages'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Description'),
        $this->t('Enabled'),
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'storage-weight',
        ],
      ],
      '#attributes' => [
        'id' => 'storage-table',
      ],
      '#empty' => $this->t('There are no storages.'),
    ];

    foreach ($storages_weights as $storage_id => $weight) {
      if (isset($cds_options[$storage_id])) {
        $form['storages'][$storage_id] = [
          '#attributes' => ['class' => ['draggable']],
          'name' => [
            '#type' => 'item',
            '#prefix' => '<strong>',
            '#markup' => $cds_options[$storage_id]['name'],
            '#suffix' => '</strong>',
          ],
          'description' => [
            '#type' => 'item',
            '#markup' => $cds_options[$storage_id]['description'],
          ],
          'enabled' => [
            '#type' => 'checkbox',
            '#default_value' => isset($storages_enabled[$storage_id]) ? 1 : 0,
          ],
          '#weight' => $weight,
          'weight' => [
            '#type' => 'weight',
            '#title' => $this->t('Weight for row @number', ['@number' => $weight + 1]),
            '#title_display' => 'invisible',
            '#default_value' => $weight,
            '#attributes' => ['class' => ['storage-weight']],
            '#delta' => count($cds_options),
          ],
          'operations' => [
            '#type' => 'operations',
            '#links' => [],
          ],
        ];

        if (isset($cds_options[$storage_id]['config_route_name'])) {
          $form['storages'][$storage_id]['operations'] = [
            '#type' => 'operations',
            '#dropbutton_type' => 'extrasmall',
            '#links' => [
              'configure' => [
                'title' => $this->t('Configure'),
                'url' => Url::fromRoute($cds_options[$storage_id]['config_route_name']),
              ],
            ],
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storages_enabled = [];
    $storages_weight = [];

    $storages = $form_state->getValue('storages');
    if (!empty($storages)) {
      foreach ($storages as $storage_id => $storage) {
        if ($storage['enabled']) {
          $storages_enabled[$storage_id] = $storage['weight'];
        }
        $storages_weight[$storage_id] = $storage['weight'];
      }

      // Ensure that the weights are integers.
      $storages_enabled = array_map('intval', $storages_enabled);
      $storages_weight = array_map('intval', $storages_weight);
    }

    $this->config('cds.settings')
      ->set('storages.enabled', $storages_enabled)
      ->set('storages.weights', $storages_weight)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
