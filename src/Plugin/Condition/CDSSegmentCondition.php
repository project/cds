<?php

namespace Drupal\cds\Plugin\Condition;

use Drupal\cds\Annotation\CustomerDataStorage;
use Drupal\cds\CustomerDataStorageHandlerInterface;
use Drupal\cds\CustomerDataStoragePluginManager;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'CDS Segment' condition.
 *
 * @Condition(
 *   id = "cds_segment_condition",
 *   label = @Translation("CDS Segment"),
 *   context = {}
 * )
 */
class CDSSegmentCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The api wrapper.
   *
   * @var \Drupal\cds\CustomerDataStorageHandlerInterface
   *    The CDS.
   */
  protected $cds;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CustomerDataStorageHandlerInterface $cds) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->cds = $cds->getStorageInstance($cds->getPrimaryStorage());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('cds')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = ['' => $this->t('None')];
    $options += $this->cds->getAllSegments();
    $form['segment'] = [
      '#type' => 'select',
      '#title' => t('Segment'),
      '#options' => $options,
      '#default_value' => isset($this->configuration['segment']) ? $this->configuration['segment'] : '',
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['segment'] = $form_state->getValue('segment');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (!$condition_segment = $this->configuration['segment']) return TRUE;

    if (isset($_COOKIE['dynamic_content_preview_segment'])) {
      $context_segments = [$_COOKIE['dynamic_content_preview_segment']];
    }
    else {
      $context_segments = $this->cds->getSegments();
      if (!$context_segments) {
        $context_segments = [];
      }
    }
    return in_array($condition_segment, $context_segments);
  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
    // TODO: Implement summary() method.
  }

  public function getCacheContexts() {
    return [
      'cds_segments',
    ];
  }

}
